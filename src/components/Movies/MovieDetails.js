import React, {Component} from 'react';

import StarshipList from '../../containers/StarshipContainer/StarshipList'
import styled from 'styled-components';

const Wrapper = styled.section`
    width: 80%;
    padding: 10px;
    font-size: 1.2rem;
`

const StarshipListStyle = styled.section`
    display: flex;
`

const MovieDetails = props => {
    return (
        <Wrapper>
            <h1>{props.movieDetails.title}</h1>
            <p>{props.movieDetails.opening_crawl}</p>
            <StarshipListStyle>
                <StarshipList starShips={props.starShips}/>
            </StarshipListStyle>
        </Wrapper>
    )
}

export default MovieDetails;