import React, { Component } from 'react';

import MovieDetails from '../../components/Movies/MovieDetails';
import styled from 'styled-components';

const Wrapper = styled.section`
    width: 100%;
    display: flex;
    justify-content: space-between;
`;

const MovieTitleStyle = styled.section`
    width: 20%;
    padding: 5px;
`

const MovieDetailStyle = styled.section`
    width: 90%;
    padding: 5px;
`


class MovieTitle extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showDetails: false,
            movieDetails: '',
            starShips: [],
        }
    }

    getMovieDescription = (episodeId) => {
        const movieDetails = this.props.moviesArray.find(episode => episode.episode_id === episodeId);
        this.setState({
            showDetails: true,
            movieDetails: movieDetails,
            starShips: movieDetails.starships
        })
    }

    renderTheMovies = () => {
        return this.props.moviesArray.map(movie => {
            return (
                <div className="list" key={movie.episode_id}>
                    <ul>
                        <li onClick={() => this.getMovieDescription(movie.episode_id)}>{movie.title}</li>
                    </ul>
                </div>
            )
        })
    }

    render() {
        return (
            <Wrapper>
                <MovieTitleStyle>
                    <h3>Movies</h3>
                    {this.renderTheMovies()}
                </MovieTitleStyle>
                <MovieDetailStyle>
                    {this.state.showDetails ? <MovieDetails movieDetails={this.state.movieDetails} starShips={this.state.starShips} /> : null}
                </MovieDetailStyle>
            </Wrapper>
        )
    }
}

export default MovieTitle;